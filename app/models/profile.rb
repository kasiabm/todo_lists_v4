class Profile < ActiveRecord::Base
	belongs_to :user
	validates :gender, :inclusion => {:in => ["male", "female"]}
	validate :first_and_last_name_cannot_be_nil, :male_cannot_be_called_sue
  
	def first_and_last_name_cannot_be_nil 
		if first_name.nil? && last_name.nil?
			errors.add(:first_name, "First name and last name cannot be both nil.")
		end
	end
  
	def male_cannot_be_called_sue
		if gender == "male" && first_name == "Sue"
			errors.add(:gender, "You're a boy, you can't be called Sue.")
		end
	end

	def self.get_all_profiles(min, max)
		Profile.where("birth_year BETWEEN :min_year AND :max_year", min_year: min, max_year: max).order(birth_year: :asc)
	end
end
