json.array!(@models) do |model|
  json.extract! model, :id, :Profile, :gender, :birth_year, :first_name, :last_name, :user_id
  json.url model_url(model, format: :json)
end
